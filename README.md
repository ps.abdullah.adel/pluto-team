# PLUTO team manual guide / [ProgressSoft](https://www.progressoft.com)

###### :man: Auther : [KNIGHTK0DER](https://github.com/mrabusalah)
------------------------------

### Plugins :
##### we should use some plugins to manage our project well, such as
- Code Formtter plugin (Maven Plugin that formats a project's Java Code following google-java-format)
    ```
        <dependency>
            <groupId>com.coveo</groupId>
            <artifactId>fmt-maven-plugin</artifactId>
            <version>2.10</version>
        </dependency>
    ```
        
- Test Coverage pluign (The JaCoCo Maven Plugin provides the JaCoCo runtime agent to your tests and allows basic report creation) 

    ```
        <plugin>
            <groupId>org.jacoco</groupId>
            <artifactId>jacoco-maven-plugin</artifactId>
            <version>${jacoco.version}</version>
            <configuration>
                <excludes>
                    <exclude>**/*Config*.*</exclude>
                    <exclude>**/*Parameters.*</exclude>
                    <exclude>**/model/</exclude>
                    <exclude>**/constants/</exclude>
                    <exclude>**/dto/</exclude>
                    <exclude>**/*Exception*.*/</exclude>
                    <exclude>**/*Entity*.*/</exclude>
                    <exclude>**/*MapperImpl.*/</exclude>
                    <exclude>**/*Resource.*</exclude>
                    <exclude>*IntegrationTest</exclude>
                    <exclude>**/liquibase/**</exclude>
                </excludes>
                <destFile>${sonar.jacoco.reportPath}</destFile>
                <append>true</append>
            </configuration>
            <executions>
                <execution>
                    <id>prepare-agent</id>
                    <goals>
                        <goal>prepare-agent</goal>
                    </goals>
                </execution>
                <execution>
                    <id>prepare-integration-agent</id>
                    <goals>
                        <goal>prepare-agent-integration</goal>
                    </goals>
                </execution>
                <execution>
                    <id>report-integration</id>
                    <phase>prepare-package</phase>
                    <goals>
                        <goal>report-integration</goal>
                    </goals>
                </execution>
                <execution>
                    <id>post-unit-test</id>
                    <phase>test</phase>
                    <goals>
                        <goal>report</goal>
                    </goals>
                </execution>
                <execution>
                    <id>post-integration-test</id>
                    <phase>integration-test</phase>
                    <goals>
                        <goal>report-integration</goal>
                    </goals>
                </execution>
                <execution>
                    <id>check-tests</id>
                    <goals>
                        <goal>check</goal>
                    </goals>
                    <configuration>
                        <dataFile>target/jacoco.exec</dataFile>
                        <rules>
                            <rule>
                                <element>BUNDLE</element>
                                <limits>
                                    <limit>
                                        <counter>INSTRUCTION</counter>
                                        <value>COVEREDRATIO</value>
                                        <minimum>${jacoco.coverageRatio}</minimum>
                                    </limit>
                                    <limit>
                                        <counter>LINE</counter>
                                        <value>COVEREDRATIO</value>
                                        <minimum>${jacoco.coverageRatio}</minimum>
                                    </limit>
                                    <limit>
                                        <counter>METHOD</counter>
                                        <value>COVEREDRATIO</value>
                                        <minimum>${jacoco.coverageRatio}</minimum>
                                    </limit>
                                    <limit>
                                        <counter>CLASS</counter>
                                        <value>COVEREDRATIO</value>
                                        <minimum>${jacoco.coverageRatio}</minimum>
                                    </limit>
                                </limits>
                            </rule>
                        </rules>
                    </configuration>
                </execution>
            </executions>
        </plugin>
    ```
- Enforcer Plugin (A plugin to manage dependencies versions)
    ```
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-enforcer-plugin</artifactId>
            <version>3.0.0-M3</version>
            <executions>
                <execution>
                    <id>enforce</id>
                    <configuration>
                        <rules>
                            <dependencyConvergence/>
                        </rules>
                    </configuration>
                    <goals>
                        <goal>enforce</goal>
                    </goals>
                </execution>
            </executions>
        </plugin>
    ```

### :scroll: These steps should be done before open a merge request
------------------------------

- First of all, create a branch from the story itself without merge request (it is recommended to name the branch by the story number).
- Before you start working on the task be sure that you checked out to your new branch.
- Reload all maven projects.
- Rum `mvn clean install` to generate all the needed classes and files.
- Try to apply the clean code principles, and if you found something should be refactor don't wait and do ti even if it's not in your task scope.
- Click `alt + shift + L` after each change to format your code and remove the unused imports on every class.
- Make sure to write a JUNIT test that covers 100% of the written production code.
- Don't forgot the Integration test as well.
- Make sure that your fix or feature is working well by testing it by integration test, postman or live site.
- Run `mvn fmt:format` if plugin exist.
- Run `mvn clean install` before pushing.
- Never push your changes by console.
- Use `ctrl + K` to review the added and changed files before pushing.
- Make sure to not push the target folders or the generated files.
- Double check the changes from the pipeline on Gitlab.
- Come back to this guide every time to be sure there is no step missing.
- open a merge request.
- If any comments added to your merge by the re-viewer apply the steps again.
- Always "leave the code better than you found it".
